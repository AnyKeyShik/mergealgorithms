#ifndef SORTS_HPP
#define SORTS_HPP

#include "merge.hpp"

template <typename T>
double mergeTDSort(std::vector<T> &data, int left, int right) {
    clock_t t1, t2;

    t1 = clock();

    if(left < right) {
        int mid = (left + right) / 2;

        mergeTDSort(data, left, mid);
        mergeTDSort(data, mid + 1, right);
        classic_merge(data, left, mid, right);
    }

    t2 = clock();

    return static_cast<double>(t2 - t1) / static_cast<double>CLOCKS_PER_SEC;
}

template <typename T>
double mergeBUSort(std::vector<T> &data, int left, int right) {
    clock_t t1, t2;

    t1 = clock();

    for (int curr_size = 1; curr_size <= right; curr_size = 2 * curr_size) {
        for (int left_start = left; left_start <= right; left_start += 2 * curr_size) {
            abstract_merge(data, left_start, min(left_start + curr_size - 1, right - 1),
                           min(left_start + curr_size + curr_size - 1, right - 1));
        }
    }

    t2 = clock();

    return static_cast<double>(t2 - t1) / static_cast<double>CLOCKS_PER_SEC;
}

template<typename T>
double mergeBatcherSort(std::vector<T> &data) {
    Array<T> s(data);

    return mergeBatcherSort(s);
}

template<typename T>
double mergeBatcherSort(Array<T> data) {
    clock_t t1, t2;

    t1 = clock();

    if (data.size() <= 2) {
        sortTwo(data);
    } else {
        std::pair< Array<T>, Array<T> > halves = data.halves();
        mergeBatcherSort(halves.first);
        mergeBatcherSort(halves.second);
        odd_even_merge(data);
    }

    t2 = clock();

    return static_cast<double>(t2 - t1) / static_cast<double>CLOCKS_PER_SEC;
}

template<typename T>
double mergeInsertionSort(std::vector<T> &data) {
    clock_t t1, t2;
    std::vector<std::pair<T, T> > pairs;
    int size = data.size();

    t1 = clock();

    for (auto i = 0; i < data.size(); i += 2) {
        sortTwo(data[i], data[i + 1]);
        pairs.push_back(std::pair<T, T>(data[i], data[i + 1]));
    }
    std::sort(pairs.begin(), pairs.end());

    data.clear();

    for (auto i = 0; i < size / 2; ++i) {
        data.push_back(pairs[i].first);
    }
    data.push_back(pairs.back().second);

    for (auto i = 0; i < size / 2 - 1; ++i) {
        int position = binSearch(data, pairs[i].second, 0, static_cast<int>(data.size()));

        if (position > data.size()) {
            data.push_back(pairs[i].second);
        } else {
            data.insert(data.begin() + position, pairs[i].second);
        }
    }

    t2 = clock();

    pairs.clear();

    return static_cast<double>(t2 - t1) / static_cast<double>CLOCKS_PER_SEC;
}

template<typename T>
double mergeBinarySort(std::vector<T> &data, int m, int n) {
    clock_t t1, t2;

    t1 = clock();

    std::sort(data.begin(), data.begin() + m);
    std::sort(data.begin() + m, data.begin() + m + n);

    binaryMerge(data, m, n);

    t2 = clock();

    return static_cast<double>(t2 - t1) / static_cast<double>CLOCKS_PER_SEC;
}

#endif //SORTS_HPP
