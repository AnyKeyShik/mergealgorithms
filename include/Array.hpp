#ifndef ARRAY_HPP
#define ARRAY_HPP

#include <vector>
#include <cassert>

template<typename T>
class Array {
private:
    std::vector<T> *m_data;
    size_t m_start, m_step, m_len;
    bool m_is2pot; /* value may be false even if exists k: m_len = 2^k */

    Array(std::vector<T> *data, size_t start, size_t step, size_t len, bool is2pot)
            : m_data(data), m_start(start), m_step(step), m_len(len), m_is2pot(is2pot) {}

public:
    explicit Array(std::vector<T> &data)
            : m_data(&data), m_start(0), m_step(1), m_len(data.size()), m_is2pot(false) {}

    Array()
            : m_data(0), m_start(0), m_step(0), m_len(0), m_is2pot(false) {}

    T& operator[](size_t i) {
        assert(m_data);
        assert(i < m_len);
        return (*m_data)[m_start + i*m_step];
    }

    const T& operator[](size_t i) const {
        assert(m_data);
        assert(i < m_len);
        return (*m_data)[m_start + i*m_step];
    }

    size_t size() {
        return m_len;
    }

    Array<T> even() {
        return Array<T>(m_data, m_start, 2 * m_step, (m_len + 1) / 2, m_is2pot);
    }

    Array<T> odd() {
        return Array<T>(m_data, m_start + m_step, 2 * m_step, m_len / 2, m_is2pot);
    }

    /* first "half" has always a power-of-two size, otherwise odd_even_merge doesn't work */
    /* if input is guaranteed to have a power-of-two size this function can get a simpler implementation */
    std::pair< Array<T>, Array<T> > halves() {
        if (1 >= m_len) {
            return std::make_pair(*this, Array<T>());
        }
        if (m_is2pot) {
            return std::make_pair(Array<T>(m_data, m_start, m_step, m_len / 2, true), Array<T>(m_data, m_start + m_step * (m_len / 2), m_step, m_len / 2, true) );
        }

        size_t len = 2;
        while (len < m_len) {
            len <<= 1;
        }
        len >>= 1;

        return std::make_pair(Array<T>(m_data, m_start, m_step, len, true), Array<T>(m_data, m_start + len * m_step, m_step, m_len - len, m_len == 2 * len ) );
    }
};

#endif //ARRAY_HPP
