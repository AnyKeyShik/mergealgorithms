#ifndef UTILS_HPP
#define UTILS_HPP

#include <iostream>
#include <iterator>

const int DELIMITER = 100;

template<typename Item>
void sortTwo(Array<Item> &data) {
    if (data.size() == 2) {
        if (data[0] > data[1]) {
            std::swap(data[0], data[1]);
        }
    }
}

template<typename Item>
void sortTwo(Item &first, Item &second) {
    if (first > second) {
        std::swap(first, second);
    }
}

template<typename Item>
int binSearch(std::vector<Item> &data, Item &item, int left, int right) {
    if (right <= left)
        return (item > data[left]) ? (left + 1) : left;

    int mid = (left + right) / 2;

    if (item == data[mid])
        return mid + 1;

    if (item > data[mid])
        return binSearch(data, item, mid + 1, right);
    return binSearch(data, item, left, mid - 1);
}

template <typename Item>
Item min(Item A, Item B) {
    return (A < B) ? A : B;
}

template <typename Item>
void print(const std::string &text, std::vector<Item> &data) {
    std::cout << text << ": ";
    copy(data.begin(), data.end(), std::ostream_iterator<Item>(std::cout, " "));
    std::cout << "\n";
}

template <typename Item>
void print(const std::string &text, double time, std::vector<Item> &data) {
    std::cout << text << ": ";
    copy(data.begin(), data.end(), std::ostream_iterator<Item>(std::cout, " "));
    std::cout << "\n";
    std::cout << "Time taken: " << time << "s\n";
    std::cout << "\n" << std::string(DELIMITER, '=') << "\n";
}


#endif //UTILS_HPP
