#ifndef MERGE_HPP
#define MERGE_HPP

#include <ctime>
#include "Array.hpp"
#include "utils.hpp"

template<typename Item>
void classic_merge(std::vector<Item> &array, int left, int mid, int right)
{
    Item *temp = new Item[right + 1];
    int i = left;
    int j = mid + 1;
    int k = 0;

    while (i <= mid && j <= right) {
        if (array[i] <= array[j])
            temp[k++] = array[i++];
        else
            temp[k++] = array[j++];
    }
    while (i <= mid)
        temp[k++] = array[i++];
    while (j <= right)
        temp[k++] = array[j++];

    k--;
    while (k >= 0) {
        array[k + left] = temp[k];
        k--;
    }

    delete[] temp;
}

template<typename Item>
void abstract_merge(std::vector<Item> &array, int left, int mid, int right)
{
    int i, j;
    std::vector<Item> aux(right + 1);

    for(i = mid + 1; i > left; i--) {
        aux[i - 1] = array[i - 1];
    }
    for(j = mid; j < right; j++) {
        aux[right + mid - j] = array[j + 1];
    }

    for(int k = left; k <= right; k++) {
        if(aux[j] < aux[i]) {
            array[k] = aux[j--];
        }
        else {
            array[k] = aux[i++];
        }
    }
}


template<typename T>
void odd_even_merge(Array<T> data) {
    if (data.size() <= 2) {
        sortTwo(data);
    } else {
        odd_even_merge(data.odd());
        odd_even_merge(data.even());
        for (size_t i = 2, len = data.size(); i < len; i += 2) {
            if (data[i-1] > data[i]) std::swap(data[i-1], data[i]);
        }
    }
}

template<typename Item>
void binaryMerge(std::vector<Item> &data, int m, int n) {
    int start = 0;
    int start2 = m;

    if (data[m - 1] < data[m]) {
        return;
    }

    while (start < m && start2 < m + n) {
        if (data[start] <= data[start2]) {
            start++;
        } else {
            int value = data[start2];
            int index = start2;

            while (index != start) {
                data[index] = data[index - 1];
                index--;
            }
            data[start] = value;

            start++;
            m++;
            start2++;
        }
    }
}

#endif //MERGE_HPP
