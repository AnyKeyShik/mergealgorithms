#include <random>
#include <algorithm>


#include "include/sorts.hpp"
#include "include/utils.hpp"

const int SIZE = 10000;

int main() {
    double time;

    std::cout << std::fixed;
    std::random_device rd;
    std::mt19937 g(rd());

    std::vector<double> data(SIZE);
    for (size_t i = 0; i < SIZE; i++) {
        data[i] = i;
    }

    std::shuffle(data.begin(), data.end(), g);
    print("Original array", data);
    time = mergeTDSort(data, 0, SIZE - 1);
    print("Array sorted by top-down MergeSort and merging with classic merge", time, data);

    std::shuffle(data.begin(), data.end(), g);
    print("Original array", data);
    time = mergeBUSort(data, 0, SIZE - 1);
    print("Array sorted by bottom-up MergeSort and merging with abstract merge", time, data);

    std::shuffle(data.begin(), data.end(), g);
    print("Original array", data);
    time = mergeBatcherSort(data);
    print("Array sorted by Batcher MergeSort and merging with Batcher merge", time, data);

    std::shuffle(data.begin(), data.end(), g);
    print("Original array", data);
    time = mergeInsertionSort(data);
    print("Array sorted by Merge-Insertion or Ford-Johnson algorithm", time, data);

    std::shuffle(data.begin(), data.end(), g);
    print("Original array", data);
    time = mergeBinarySort(data, 4, 6);
    print("Array sorted by BinaryMerge", time, data);
}
